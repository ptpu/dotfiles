# dotfiles

To add new config files to repository, create similiar folder and files and then define symlink using

```
stow --adopt -vt ~ *
```

After cloning, add config files to system using
```
stow -vSt ~ *
```